//
//  ViewController.swift
//  SampleLegacyApp
//
//  Created by Nuwan Abeysinghe on 5/8/20.
//  Copyright © 2020 Nuwan Abeysinghe. All rights reserved.
//

import UIKit
import livechat

public let NUWAN:String = "2c92809d71ca1b840171cae021000002";

public let NUWAN_TOKEN:String = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjJjOTI4MDlkNzFjYTFiODQwMTcxY2FlMDIxMDAwMDAyIiwibmFtZSI6Im51d2FuQHRldGhlcmZpLmNvbSIsIm9yZ2FuaXphdGlvbiI6IlByb2R1Y3RRQSIsImlhdCI6MTU5NDg0MzMxNiwiZXhwIjoxNTk1MDE2MTE2LCJhdWQiOiJudXdhbkB0ZXRoZXJmaS5jb20iLCJpc3MiOiJndWFyZGlhbi9vcmdhbml6YXRpb24iLCJzdWIiOiJudXdhbkB0ZXRoZXJmaS5jb20iLCJqdGkiOiJjMjlmYjVkYi02YWU4LTRmOGEtYTk5NC00YmVhZWI0YThiNWEifQ._JXbHbdJxYp-tfj63nH9PjsLwn9vF0qyBnkkyFomIkY";

public let SIG_URL:String = "wss://proxy.qa.tetherfi.cloud/ws-chat-proxy/ws/chat?token=" + NUWAN_TOKEN;

//===

public let AMY:String = "2c92809671c9caf00171c9e964f40002";
public let COCKE:String = "2c92809c71d5213c0171deeb5dce0006";

//===

public let CONFIGS:[String : Any] = [
    
        "type": "configs",
        "ice": Array<Any>(
            arrayLiteral: [
                "url": "turn:proxy.lab.tetherfi.cloud:3478?transport=tcp",
                "user": "tetherfi",
                "pass": "tetherfi"
                ],
                [
                "url": "turn:3.233.143.234:3478",
                "user": "tetherfi",
                "pass": "tetherfi"
                ]
            ),
        "avmode": "audio",
        "video": [
            "width": 720,
            "height": 720,
            "fps": 25
        ],
        "media_conn_mode": "all",
        "mirror_selfview": true,
        "android_manage_audio": true,
        "prompt_avrequest": true,
        "sdp_mung": [
            "enabled": true,
            "audio_limit": 32,
            "video_limit": 128,
            "session_limit": 1024,
            "vpcodec": 9
        ],
        "detect_gsm": false,
        "sdpPlan" : "plan-b",
        //"sdpPlan" : "unified-plan"
];


class ViewController: UIViewController {
    
    @IBOutlet var m_UserId:UITextField!;
    @IBOutlet var m_Token:UITextField!;
    @IBOutlet var m_Url:UITextField!;
    @IBOutlet var m_Remote:UITextField!;
    @IBOutlet var m_IsVideo:UISwitch!;
    @IBOutlet var m_VideoLayout:UIStackView!;
    
    let app:SampleAppLogic = SampleAppLogic();

    override func viewDidLoad() {
        super.viewDidLoad()
        m_UserId.text = NUWAN;
        m_Token.text = NUWAN_TOKEN;
        m_Url.text = SIG_URL;
        m_Remote.text = COCKE;
        m_IsVideo.setOn( false, animated: true );
        
        app.SetConfigs(json: CONFIGS );
    }
    
    @IBAction func onClickStart()
    {
        //let v:SignView = SignView( /*frame: m_VideoLayout.bounds*/ )!;
        //m_VideoLayout.addSubview( v );
        
        app.Start(userid: m_UserId.text!, token: m_Token.text!, url: m_Url.text!);
    }
    
    @IBAction func onClickEnd()
    {
        app.End(reason: "Sig End by user" );
    }
    
    @IBAction func onClickStartAV()
    {
        if( m_IsVideo.isOn )
        {
            app.InitVideo(parent: m_VideoLayout );
        }
        
        app.StartAv(isVideo: m_IsVideo.isOn, remoteId: m_Remote.text!);
        
    }
    
    @IBAction func onClickEndAV()
    {
        app.EndAv(reason: "AV End by User");
    }

}

