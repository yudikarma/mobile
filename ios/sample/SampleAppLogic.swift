//
//  AppLogic.swift
//  SampleApp
//
//  Created by Nuwan Abeysinghe on 4/8/20.
//  Copyright © 2020 Nuwan Abeysinghe. All rights reserved.
//

import Foundation
import livechat
import WebKit

class VideoHandler : IAvListener
{
    let m_Parent:UIView;
    
    init( parent:UIView ) {
        m_Parent = parent;
    }
    
    func OnVideoViewAdded(view: UIView, type: String, id: String)
    {
        //view.setValue( id, forKey: "lvchatvideo");
        m_Parent.addSubview( view );
    }
    
    func OnVideoViewRemoved(id: String) {
        /*
        m_Parent.subviews.forEach { (v) in
            let iid:String? = v.value(forKey: "lvchatvideo") as! String?;
            if( iid == nil ){ return };
            
            if( iid == id )
            {
                v.removeFromSuperview();
            }
        }
 */
    }
}

class SampleAppLogic
{
    let api:Livechat2Api;
    var call:CallSession?;

    
    init() {
        api = Livechat2Api();
    }
    
    public func Start( userid:String, token:String, url:String )
    {
        api.Initialize(url: url );
        api.setUserAuth(userId: userid, auth: token);
        call = CallSession(userid: userid);
        api.start(userObject: "", fetch: true);
    }
    
    public func SetConfigs( json:[AnyHashable : Any] )
    {
        Configs.instance()?.config(byJSON: json);
    }
    
    public func InitVideo( parent:UIView )
    {
        call?.SetVideoEventHandler(videoEventHandler:  VideoHandler( parent: parent ) );
    }
    
    public func StartAv( isVideo:Bool, remoteId:String )
    {
        
        
        let remoteObj = "{ \"id\":\"" + remoteId + "\", \"title\":\"testName\", \"remoteAddr\":\"user:" + remoteId + "\" }";
        call?.StartAv(mode: (isVideo ? "video": "audio" ), remoteUserObj: remoteObj );
    }
    
    public func EndAv( reason:String )
    {
        call?.EndAv(reason: reason );
    }
    
    public func End( reason:String )
    {
        api.end(reason: reason );
    }
    
    
    
}
