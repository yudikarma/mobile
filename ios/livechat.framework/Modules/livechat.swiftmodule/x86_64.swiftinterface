// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.3.1 (swiftlang-1200.0.41 clang-1200.0.32.8)
// swift-module-flags: -target x86_64-apple-ios11.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -Osize -module-name livechat
import CallKit
import Foundation
import Swift
import UIKit
import WebKit
@_exported import livechat
import sigv2
@objc public protocol AvCallApi {
  @objc func SetAvEventsHandler(handler: livechat.Livechat_AvCallEventsHandler)
  @objc func SetSignallingApi(sigApi: livechat.SignallingApi)
  @objc func ReceiveAppMessage(msg: Swift.String) -> Swift.Bool
  @objc func ReceiveAppMessage(json: [Swift.AnyHashable : Any]?) -> Swift.Bool
  @objc func StartAvCall(avmode: livechat.AVMode) -> Swift.Bool
  @objc func RequestAvCall(avmode: Swift.String)
  @objc func EndAvCall(reason: Swift.String)
  @objc func PauseAvCall()
  @objc func ResumeAvCall()
  @objc func MuteAudio()
  @objc func UnmuteAudio()
  @objc func AvResponse(avmode: Swift.String)
  @objc func TryReconnect()
}
@objc public class Logger : ObjectiveC.NSObject {
  public static var outputClosure: (Swift.String) -> ()
  public static func SetOutCallback(method: @escaping (Swift.String) -> Swift.Void)
  public init(name: Swift.String)
  public func error(_ msg: Swift.String)
  public func warn(_ msg: Swift.String)
  public func info(_ msg: Swift.String)
  public func debug(_ msg: Swift.String)
  @objc public static func objc_log(time: Swift.String, type: Swift.String, currentSid: Swift.String, cls: Swift.String, msg: Swift.String)
  @objc deinit
  @objc override dynamic public init()
}
public class AvCallApiImpl : livechat.AvCallApi, livechat.Signalling {
  public init()
  @objc public func sendSigMessage(_ message: Swift.String)
  @objc public func SetAvEventsHandler(handler: livechat.Livechat_AvCallEventsHandler)
  @objc public func SetSignallingApi(sigApi: livechat.SignallingApi)
  @objc public func ReceiveAppMessage(json: [Swift.AnyHashable : Any]?) -> Swift.Bool
  @objc public func ReceiveAppMessage(msg: Swift.String) -> Swift.Bool
  @objc public func StartAvCall(avmode: livechat.AVMode) -> Swift.Bool
  @objc public func RequestAvCall(avmode: Swift.String)
  @objc public func EndAvCall(reason: Swift.String)
  @objc public func PauseAvCall()
  @objc public func ResumeAvCall()
  @objc public func MuteAudio()
  @objc public func UnmuteAudio()
  @objc public func AvResponse(avmode: Swift.String)
  @objc public func TryReconnect()
  @objc deinit
}
public class Livechat2Api : sigv2.SigListener {
  public init()
  public func Initialize(url: Swift.String)
  @objc public func onEnd(reason: Swift.String)
  @objc public func onServerStateChanged(state: Swift.String)
  public func registerListener(type: sigv2.MessageType, regid: Swift.String, listener: @escaping ((Swift.String?, Swift.String?, Swift.String?) -> Swift.Void))
  public func unregisterListener(type: sigv2.MessageType, regid: Swift.String)
  public func setUserAuth(userId: Swift.String, auth: Swift.String)
  public func start(userObject: Swift.String, fetch: Swift.Bool)
  public func sendLv2Message(type: sigv2.MessageType, content: Swift.String, to: sigv2.SysAddr, onSent: ((Swift.Bool, Swift.String) -> Swift.Void)) -> Swift.Int
  public func end(reason: Swift.String)
  @objc deinit
}
public class CallSession {
  public init(userid: Swift.String)
  public typealias EventHandler = ((livechat.EventIndex, Swift.String?, Swift.String?) -> Swift.Void)
  public func AddCallEventHandler(index: livechat.EventIndex, regid: Swift.String, callback: @escaping livechat.CallSession.EventHandler)
  public func SetVideoEventHandler(videoEventHandler: livechat.IAvListener)
  public func RemoveHandler(index: livechat.EventIndex, regid: Swift.String)
  public func StartAv(mode: Swift.String, remoteUserObj: Swift.String)
  public func EndAv(reason: Swift.String)
  public func SetUserResponse(type: Swift.String, param: Swift.String)
  @objc deinit
}
@objc public protocol SignallingApi {
  @objc func SetSessionEventsHandler(handler: livechat.Livechat_SessionEventsHandler)
  @objc func SetAvCallApi(avcallApi: livechat.AvCallApi)
  @objc func SetUserAuth(userId: Swift.String, auth: Swift.String)
  @objc func Start(custDetails: Swift.String, vaSession: Swift.String)
  @objc func End(reason: Swift.String)
  @objc func Open()
  @objc func GetSigSessionId() -> Swift.String?
  @objc func SendMessage(type: Swift.String, msg: Swift.String, to: Swift.String, onSent: ((Swift.Bool, Swift.String) -> Swift.Void)?) -> Swift.Int
}
public typealias SignallingApi_ISendCallback = ((Swift.Bool, Swift.String) -> Swift.Void)
@objc public class Livechat : ObjectiveC.NSObject {
  public init(extObj: ObjectiveC.NSObject)
  public func SetLogger(outLogFunc: @escaping ((Swift.String) -> Swift.Void))
  public func SetSessionEventsHandler(handler: livechat.Livechat_SessionEventsHandler)
  public func SetAvEventsHandler(handler: livechat.Livechat_AvCallEventsHandler)
  public func SetUserAuth(user: Swift.String, auth: Swift.String)
  public func Start(sessionDetails: livechat.Livechat_SessionDetails, vaSession: Swift.String)
  public func End(reason: Swift.String)
  public func Open()
  public func SendUserMessage(msg: Swift.String, user: Swift.String, onSent: @escaping livechat.SignallingApi_ISendCallback) -> Swift.Int
  public func NotifyTypingStateChange(state: Swift.Int, user: Swift.String)
  public func StartVideoCall() -> Swift.Bool
  public func StartAudioCall() -> Swift.Bool
  public func TryReconnect()
  public func RequestAvCall(avmode: Swift.String)
  public func EndAvCall(reason: Swift.String)
  public func PauseAvCall()
  public func ResumeAvCall()
  public func AvResponse(avmode: Swift.String)
  public func MuteAudio()
  public func UnmuteAudio()
  @objc deinit
  @objc override dynamic public init()
}
@objc public protocol Livechat_SessionEventsHandler {
  @objc func OnStartSuccess(sid: Swift.String, tchatid: Swift.String, param: Swift.String)
  @objc func OnStartFailed(errorCode: Swift.String)
  @objc func OnUserMessage(json: [Swift.AnyHashable : Any]?, fromUser: Swift.String)
  @objc func OnOpenSuccess(sessionDesc: Swift.String)
  @objc func OnOpenFailed(errorCode: Swift.String)
  @objc func OnEnd(initiator: Swift.String, reason: Swift.String)
  @objc func OnCSOJoin(name: Swift.String, userid: Swift.String, rsessionId: Swift.String)
  @objc func OnRequestQueued(position: Swift.Int, reqid: Swift.String)
  @objc func OnQueuePositionUpdated(position: Swift.Int, reqid: Swift.String)
  @objc func OnSignallingStateChanged(state: Swift.String)
  @objc func OnCSOLeft(name: Swift.String, userid: Swift.String)
  @objc func OnTypingState(state: Swift.Int, fromUser: Swift.String)
  @objc func OnCallbackRequestStatus(status: Swift.Bool, selfRequest: Swift.Bool)
  @objc func OnCustomMessage(type: Swift.String, content: [Swift.AnyHashable : Any]?, from: Swift.String)
}
@objc public protocol Livechat_AvCallEventsHandler {
  @objc func OnRequestAvCall(avmode: Swift.String)
  @objc func OnRespondAvCall(avmode: Swift.String)
  @objc func OnLocalVideo(localVideoView: UIKit.UIView)
  @objc func OnRemoteVideo(remoteVideoView: UIKit.UIView)
  @objc func OnAvCallStartSuccess()
  @objc func OnAvCallStartFailure(if_any: Swift.Error?)
  @objc func OnMediaDisconnect()
  @objc func OnMediaReconnect()
  @objc func OnMediaReconnectableEnd()
  @objc func OnAvCallEnd(type: livechat.EndType, reasonDesc: Swift.String)
}
@objc public class Livechat_SessionDetails : ObjectiveC.NSObject {
  public init(jsonDetails: Swift.String)
  public func ToString() -> Swift.String
  @objc deinit
  @objc override dynamic public init()
}
public protocol IAvListener {
  func OnVideoViewAdded(view: UIKit.UIView, type: Swift.String, id: Swift.String)
  func OnVideoViewRemoved(id: Swift.String)
}
@objc public class LivechatWebview : ObjectiveC.NSObject, WebKit.WKNavigationDelegate {
  public init(webview: WebKit.WKWebView?, pageUrl: Swift.String, pageContentFile: Swift.String?, postLoadScriptFile: Swift.String?)
  public func addMessageHandler(wkscript: WebKit.WKScriptMessageHandler, name: Swift.String)
  public func evalJs(str: Swift.String)
  @objc public func webView(_ webView: WebKit.WKWebView, didReceive challenge: Foundation.URLAuthenticationChallenge, completionHandler: @escaping (Foundation.URLSession.AuthChallengeDisposition, Foundation.URLCredential?) -> Swift.Void)
  @objc deinit
  @objc override dynamic public init()
}
@objc public class LivechatAvSession : ObjectiveC.NSObject, livechat.LivechatApi, WebKit.WKUIDelegate {
  public init(webview: WebKit.WKWebView?, handler: livechat.LivechatApi_Events)
  @objc public func Start(custDetailsJsonObject: Swift.String, history: Swift.String)
  @objc public func Open()
  @objc public func End(reason: Swift.String)
  @objc public func SendMessage(msg: Swift.String)
  @objc public func SendAppMessage(msg: Swift.String)
  @objc public func NotifyTypingStateChange(state: Swift.Int)
  @objc public func SetConfigs(configs: Swift.String)
  @objc public func StartAv()
  @objc public func EndAv()
  @objc public func PauseAv()
  public static func logi(msg: Swift.String)
  @objc deinit
  @objc override dynamic public init()
}
@objc public protocol LivechatApi {
  @objc func Start(custDetailsJsonObject: Swift.String, history: Swift.String)
  @objc func End(reason: Swift.String)
  @objc func Open()
  @objc func SendMessage(msg: Swift.String)
  @objc func SendAppMessage(msg: Swift.String)
  @objc func NotifyTypingStateChange(state: Swift.Int)
  @objc func SetConfigs(configs: Swift.String)
  @objc func StartAv()
  @objc func EndAv()
  @objc func PauseAv()
}
@objc public protocol LivechatApi_Events {
  @objc func OnStartSuccess(sid: Swift.String, tchatid: Swift.String, paramObject: Foundation.NSDictionary?)
  @objc func OnAppMessageReceived(agent: Swift.String, msg: Swift.String)
  @objc func OnMessageReceived(agent: Swift.String, msg: Swift.String)
  @objc func OnStartFailed(errorCode: Swift.String)
  @objc func OnOpenSuccess(sessionDesc: Swift.String)
  @objc func OnOpenFailed(errorCode: Swift.String)
  @objc func OnEnd(initiator: Swift.String, reason: Swift.String)
  @objc func OnRemoteUserConnected(name: Swift.String, agentId: Swift.String, sessionId: Swift.String)
  @objc func OnCallQueued(position: Swift.String, extra: Swift.String?)
  @objc func OnStatusNotification(notification: Swift.String)
  @objc func OnCallbackRequestStatus(isCustomerReq: Swift.Bool, status: Swift.Bool)
}
@_inheritsConvenienceInitializers @objc public class CkHandler : ObjectiveC.NSObject, CallKit.CXProviderDelegate {
  @objc public static var Instance: livechat.CkHandler
  public static func getInstance() -> livechat.CkHandler
  @objc override dynamic public init()
  @objc public func createNewCall(rtc: livechat.RTCSession, isVideo: Swift.Bool) -> Foundation.UUID
  @objc public func providerDidReset(_ provider: CallKit.CXProvider)
  @objc public func providerDidBegin(_ provider: CallKit.CXProvider)
  @objc public func provider(_ provider: CallKit.CXProvider, perform action: CallKit.CXStartCallAction)
  @objc public func provider(_ provider: CallKit.CXProvider, perform action: CallKit.CXAnswerCallAction)
  @objc public func provider(_ provider: CallKit.CXProvider, perform action: CallKit.CXEndCallAction)
  @objc public func provider(_ provider: CallKit.CXProvider, perform action: CallKit.CXSetHeldCallAction)
  @objc public func provider(_ provider: CallKit.CXProvider, perform action: CallKit.CXSetMutedCallAction)
  @objc public func end(uuid: Foundation.UUID)
  @objc deinit
}
@objc public enum EventIndex : Swift.Int32 {
  case UNCONNECTED = 0
  case MEDIA_REQ_RECEIVED
  case PROMPT_FOR_MEDIA
  case MEDIA_REQ_SENT
  case MEDIA_REQ_ACKED
  case CONNECTING_MEDIA
  case RECONNECTING_MEDIA
  case INCALL_MEDIA
  case REINCALL_MEDIA
  case ENDED
  case AVSTART_FAILED
  case VIDEO_RECEIVED
  case CALL_TIME
  public typealias RawValue = Swift.Int32
  public init?(rawValue: Swift.Int32)
  public var rawValue: Swift.Int32 {
    get
  }
}
@_inheritsConvenienceInitializers @objc public class FactoryEx : ObjectiveC.NSObject {
  public static var Instance: livechat.FactoryEx
  public func CreateSigApi(extObj: ObjectiveC.NSObject) -> livechat.SignallingApi
  public func CreateNativeSigApi() -> livechat.SignallingApi?
  public func CreateAvCallApi() -> livechat.AvCallApi
  @objc deinit
  @objc override dynamic public init()
}
public class WebviewSigSession : livechat.SignallingApi {
  @objc public func GetSigSessionId() -> Swift.String?
  @objc public func SetUserAuth(userId: Swift.String, auth: Swift.String)
  public init(wkwebview: WebKit.WKWebView?)
  @objc public func SetSessionEventsHandler(handler: livechat.Livechat_SessionEventsHandler)
  @objc public func SetAvCallApi(avcallApi: livechat.AvCallApi)
  @objc public func Start(custDetails: Swift.String, vaSession: Swift.String)
  @objc public func End(reason: Swift.String)
  @objc public func Open()
  @objc public func SendMessage(type: Swift.String, msg: Swift.String, to: Swift.String, onSent: ((Swift.Bool, Swift.String) -> Swift.Void)?) -> Swift.Int
  public func SendAppMessage(msg: Swift.String)
  public func NotifyTypingStateChange(state: Swift.Int)
  @objc deinit
}
